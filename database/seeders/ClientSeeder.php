<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    private const COUNT = 5;

    public function run(): void
    {
        Client::factory(self::COUNT)
            ->create()
        ;
    }
}
