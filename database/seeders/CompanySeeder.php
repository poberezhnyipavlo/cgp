<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    private const COUNT = 150;

    public function run(): void
    {
        Company::factory(self::COUNT)
            ->state(new Sequence(
                fn () => ['client_id' => Client::all()->random()]
            ))
            ->create()
        ;
    }
}
