<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ClientController;
use App\Http\Controllers\Api\CompanyController;
use Illuminate\Support\Facades\Route;

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('companies', [CompanyController::class, 'getCompanies']);
    Route::get('companies/byClient/{client}', [CompanyController::class, 'getCompaniesByClient']);
    Route::get('clients', [ClientController::class, 'getClients']);
});
