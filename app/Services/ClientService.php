<?php

namespace App\Services;

use App\Dto\ClientDto;
use App\Models\Client;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ClientService
{
    public function getAll(?array $params = null): LengthAwarePaginator
    {
        return Client::query()
            ->with('companies')
            ->paginate($params['limit'] ?? 10)
        ;
    }

    public function getClientsWithoutPaginate(): Collection
    {
        return Client::query()->get();
    }

    public function create(ClientDto $clientDto): bool
    {
        $client = new Client();
        $this->initData($client, $clientDto);

        if ($client->save()) {
            return true;
        }

        return false;
    }

    public function delete(Client $client): bool
    {
        if ($client->delete()) {
            return true;
        }

        return false;
    }

    public function update(Client $client, ClientDto $clientDto): bool
    {
        $this->initData($client, $clientDto);

        if ($client->save()) {
            return true;
        }

        return false;
    }

    private function initData(Client $client, ClientDto $clientDto): void
    {
        $client->first_name = $clientDto->getFirstName();
        $client->last_name = $clientDto->getLastName();
    }
}
