<?php

namespace App\Services;

use App\Dto\CompanyDto;
use App\Models\Client;
use App\Models\Company;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class CompanyService
{
    public function getAll(?array $params = null): LengthAwarePaginator
    {
        return Company::query()
            ->with('client')
            ->paginate($params['limit'] ?? null)
       ;
    }

    public function getCompaniesByClient(Client $client, ?array $params = null): LengthAwarePaginator
    {
        return Company::query()
            ->where('client_id', $client->id)
            ->paginate($params['limit'] ?? null)
        ;
    }

    public function create(CompanyDto $companyDto): bool
    {
        $company = new Company();
        $this->initData($company, $companyDto);

        if ($company->save()) {
            return true;
        }

        return false;
    }

    public function update(Company $company, CompanyDto $companyDto): bool
    {
        $this->initData($company, $companyDto);

        if ($company->save()) {
            return true;
        }

        return false;
    }

    public function delete(Company $company): bool
    {
        if ($company->delete()) {
            return true;
        }

        return false;
    }

    private function initData(Company $company, CompanyDto $companyDto): void
    {
        $company->name = $companyDto->getName();
        $company->slug = Str::slug(substr($companyDto->getName(), 0, 20));
        $company->address = $companyDto->getAddress();
        $company->client_id = $companyDto->getClientId();
    }
}
