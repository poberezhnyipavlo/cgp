<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property string name
 * @property string slug
 * @property string address
 * @property int client_id
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 *
 * @property Client client
 *
 * Class Company
 * @package App\Models
 */
class Company extends Model
{
    use HasFactory;

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'address',
        'client_id',
    ];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}
