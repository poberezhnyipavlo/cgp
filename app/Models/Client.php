<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property string full_name
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 *
 * @property Collection|Company[] companies
 *
 * Class Client
 * @package App\Models
 */
class Client extends Model
{
    use HasFactory;

    /** @var array $fillable */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function companies(): HasMany
    {
        return $this->hasMany(Company::class);
    }
}
