<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\ClientRequest;
use App\Models\Client;
use App\Services\ClientService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ClientController extends Controller
{
    public function __construct(private ClientService $clientService) {}

    public function index(): View
    {
        $clients = $this->clientService->getAll();

        return view('cms.client.index', ['clients' => $clients]);
    }

    public function create(): View
    {
        $client = new Client();

        return view('cms.client.create', [
            'client' => $client,
        ]);
    }

    public function store(ClientRequest $request): RedirectResponse
    {
        $isCreated = $this->clientService->create($request->getDto());

        if ($isCreated) {
            return redirect()->route('clients.index');
        }

        return redirect()->back();
    }

    public function edit(Client $client): View
    {
        return view('cms.client.edit', [
            'client' => $client,
        ]);
    }

    public function update(ClientRequest $request, Client $client): RedirectResponse
    {
        $isUpdated = $this->clientService->update($client, $request->getDto());

        if ($isUpdated) {
            return redirect()->route('clients.index');
        }

        return redirect()->back();
    }

    public function destroy(Client $client): RedirectResponse
    {
        $isDeleted = $this->clientService->delete($client);

        if ($isDeleted) {
            return redirect()->route('clients.index');
        }

        return redirect()->back();
    }
}
