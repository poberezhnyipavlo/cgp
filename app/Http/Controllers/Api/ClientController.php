<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\ClientGetRequest;
use App\Http\Resources\Client\ClientCollection;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;

class ClientController extends Controller
{
    public function __construct(private ClientService $clientService) {}

    public function getClients(ClientGetRequest $request): JsonResponse
    {
        return response()->json(new ClientCollection($this->clientService->getAll($request->all())));
    }
}
