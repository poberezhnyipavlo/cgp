<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyGetRequest;
use App\Http\Resources\Company\CompanyCollection;
use App\Http\Resources\Company\CompanyWithoutClientCollection;
use App\Models\Client;
use App\Services\CompanyService;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    public function __construct(private CompanyService $companyService) {}

    public function getCompanies(CompanyGetRequest $request): JsonResponse
    {
        return response()->json(new CompanyCollection($this->companyService->getAll($request->all())));
    }

    public function getCompaniesByClient(Client $client, CompanyGetRequest $request): JsonResponse
    {
        return response()
            ->json(
                new CompanyWithoutClientCollection(
                    $this
                        ->companyService
                        ->getCompaniesByClient(
                            $client,
                            $request->all()
                        )
                )
            )
        ;
    }
}
