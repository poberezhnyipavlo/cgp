<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\CompanyGetRequest;
use App\Http\Requests\Company\CompanyStoreRequest;
use App\Http\Requests\Company\CompanyUpdateRequest;
use App\Models\Company;
use App\Services\ClientService;
use App\Services\CompanyService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CompanyController extends Controller
{
    public function __construct(
        private CompanyService $companyService,
        private ClientService $clientService,
    ) {}

    public function index(CompanyGetRequest $request): View
    {
        $companies = $this->companyService->getAll($request->all());

        return view('cms.company.index', ['companies' => $companies]);
    }

    public function create(): View
    {
        $company = new Company();
        $clients = $this->clientService->getClientsWithoutPaginate();

        return view('cms.company.create', [
            'company' => $company,
            'clients' => $clients,
        ]);
    }

    public function store(CompanyStoreRequest $request): RedirectResponse
    {
        $isCreated = $this->companyService->create($request->getDto());

        if ($isCreated) {
            return redirect()->route('companies.index');
        }

        return redirect()->back();
    }

    public function edit(Company $company): View
    {
        $clients = $this->clientService->getClientsWithoutPaginate();

        return view('cms.company.edit', [
            'company' => $company,
            'clients' => $clients,
        ]);
    }

    public function update(CompanyUpdateRequest $request, Company $company): RedirectResponse
    {
        $isUpdated = $this->companyService->update($company, $request->getDto());

        if ($isUpdated) {
            return redirect()->route('companies.index');
        }

        return redirect()->back();
    }

    public function destroy(Company $company): RedirectResponse
    {
        $isDeleted = $this->companyService->delete($company);

        if ($isDeleted) {
            return redirect()->route('companies.index');
        }

        return redirect()->back();
    }
}
