<?php

namespace App\Http\Requests\Company;

use Illuminate\Validation\Rule;

class CompanyStoreRequest extends CommonCompanyRequest
{
    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                'name' => [
                    'required',
                    'string',
                    Rule::unique('companies', 'name'),
                ],
            ]
        );
    }
}
