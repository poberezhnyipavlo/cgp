<?php

namespace App\Http\Requests\Company;

use App\Models\Company;
use Illuminate\Validation\Rule;

class CompanyUpdateRequest extends CommonCompanyRequest
{
    public function rules(): array
    {
        /** @var Company $company */
        $company = $this->route('company');

        return array_merge(
            parent::rules(),
            [
                'name' => [
                    'required',
                    'string',
                    Rule::unique('companies', 'name')->ignore($company->id),
                ],
            ],
        );
    }
}
