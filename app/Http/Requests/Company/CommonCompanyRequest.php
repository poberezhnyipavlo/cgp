<?php

namespace App\Http\Requests\Company;

use App\Dto\CompanyDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\Pure;

abstract class CommonCompanyRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'address' => [
                'nullable',
                'string',
            ],
            'client_id' => [
                'required',
                'integer',
                Rule::exists('clients', 'id'),
            ],
        ];
    }

    #[Pure]
    public function getDto(): CompanyDto
    {
        return new CompanyDto(
            name: $this->name,
            address: $this->address,
            clientId: (int) $this->client_id,
        );
    }
}
