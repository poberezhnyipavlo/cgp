<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class CommonGetRequest extends FormRequest
{
    protected const DEFAULT_LIMIT = 10;

    public function all($keys = null): array
    {
        $data = parent::all($keys);

        $data['limit'] = $data['limit'] ?? self::DEFAULT_LIMIT;

        return $data;
    }

    public function rules(): array
    {
        return [
            'page' => [
                'int',
                'min:1',
            ],
            'limit' => [
                'int',
                'min:1',
            ],
        ];
    }
}
