<?php

namespace App\Http\Requests\Client;

use App\Dto\ClientDto;
use App\Models\Client;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\Pure;

class ClientRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'first_name' => [
                'required',
                'string',
                'max:50',
                'min:3',
            ],
            'last_name' => [
                'required',
                'string',
                'max:50',
                'min:3',
            ],
        ];
    }

    #[Pure]
    public function getDto(): ClientDto
    {
        return new ClientDto(
            firstName: $this->first_name,
            lastName: $this->last_name,
        );
    }
}
