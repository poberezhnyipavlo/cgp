<?php

namespace App\Http\Resources\Company;

use App\Http\Resources\Client\CompanyResource as CompanyResourceWithoutClient;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyWithoutClientCollection extends ResourceCollection
{
    public $collection = CompanyResourceWithoutClient::class;

    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
            'paginate' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
        ];
    }
}
