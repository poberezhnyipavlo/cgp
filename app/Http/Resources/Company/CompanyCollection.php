<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\CollectsResources;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyCollection extends ResourceCollection
{
    public $collection = CollectsResources::class;

    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
            'paginate' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
       ];
    }
}
