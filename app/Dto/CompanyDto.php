<?php

namespace App\Dto;

final class CompanyDto
{
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getClientId(): int
    {
        return $this->clientId;
    }

    public function setClientId(int $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function __construct(
        private string $name,
        private ?string $address,
        private int $clientId,
    ) {}
}
