@extends('adminlte::page')

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Update category {{ $company->name ?? '' }}</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('companies.update', $company) }}" method="post">
                @csrf
                @method('put')
                @include('cms.company.form.form')
                <button type="submit" class="btn btn-primary">Update</button>
                <button type="reset" class="btn btn-warning">Cancel</button>
            </form>
        </div>
    </div>
@endsection