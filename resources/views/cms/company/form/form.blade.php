@php /** @var App\Models\Company $company */ @endphp
<div class="form-group row">
    <label class="col-form-label col-sm-2">Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ old('name') ?: $company->name ?? '' }}">
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label col-sm-2">Address</label>
    @error('address')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="col-sm-10">
        <input type="text" class="form-control" name="address" value="{{ old('address') ?: $company->address ?? '' }}">
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label col-sm-2">Choose a client</label>
    <div class="col-sm-10">
        <select
            class="form-control"
            name="client_id"
        >
            @foreach($clients as $client)
                @php /** @var App\Models\Client $client */ @endphp
                <option value="{{ $client->id }}"
                    @if($client->id === $company->company_id)
                        selected
                    @endif
                >
                   {{ $client->full_name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
