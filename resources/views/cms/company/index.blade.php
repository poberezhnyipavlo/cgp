@extends('adminlte::page')

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="col">
                <h1>Categories</h1>
            </div>
            <div class="mb-3">
                <a
                    type="button"
                    class="btn btn-primary"
                    href="{{ route('companies.create') }}"
                >
                    Add Category
                </a>
            </div>
            <table
                    class="table table-bordered table-hover dataTable dtr-inline"
                    role="grid"
            >
                <thead>
                    <tr role="row">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Address</th>
                        <th>Client</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($companies as $company)
                        @php /** @var App\Models\Company $company **/ @endphp
                        <tr>
                            <th>
                                {{ $company->id }}
                            </th>
                            <th>
                                <a href="{{ route('companies.edit', $company) }}">{{ $company->name }}</a>
                            </th>
                            <th>
                                {{ $company->slug }}
                            </th>
                            <th>
                                {{ $company->address }}
                            </th>
                            <th>
                                {{ $company->client->full_name }}
                            </th>
                            <th>
                                {{ $company->created_at }}
                            </th>
                            <th>
                                {{ $company->updated_at ?? '' }}
                            </th>
                            <th class="text-right">
                                <form
                                    onsubmit="return confirm('Delete category?');"
                                    action="{{ route('companies.destroy', $company) }}"
                                    method="post"
                                >
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </th>
                        </tr>
                    @empty
                        <tr>
                            <th colspan="3">No data</th>
                        </tr>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3" class="dataTables_paginate paging_simple_numbers"> {{ $companies->links() }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection