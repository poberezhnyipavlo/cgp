@php /** @var App\Models\Client $client */ @endphp
<div class="form-group row">
    <label class="col-form-label col-sm-2">First name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="first_name" value="{{ old('first_name') ?: $client->first_name ?? '' }}">
        @error('first_name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label col-sm-2">last name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="last_name" value="{{ old('last_name') ?: $client->last_name ?? '' }}">
    </div>
    @error('last_name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

