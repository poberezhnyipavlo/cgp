@extends('adminlte::page')

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Create new client</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('clients.store') }}" method="post">
                @csrf
                @include('cms.client.form.form')
                <button type="submit" class="btn btn-primary">Create</button>
                <button type="reset" class="btn btn-warning">Cancel</button>
            </form>
        </div>
    </div>
@endsection