@extends('adminlte::page')

@php /** @var App\Models\Client $client */ @endphp
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Update client {{ $client->first_name ?? '' }}</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('clients.update', $client) }}" method="post">
                @csrf
                @method('put')
                @include('cms.client.form.form')
                <button type="submit" class="btn btn-primary">Update</button>
                <button type="reset" class="btn btn-warning">Cancel</button>
            </form>
        </div>
    </div>
@endsection