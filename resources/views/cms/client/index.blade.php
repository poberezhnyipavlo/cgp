@extends('adminlte::page')

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="col">
                <h1>Clients</h1>
            </div>
            <div class="mb-3">
                <a
                    type="button"
                    class="btn btn-primary"
                    href="{{ route('clients.create') }}"
                >
                    Add Client
                </a>
            </div>
            <table
                    class="table table-bordered table-hover dataTable dtr-inline"
                    role="grid"
            >
                <thead>
                    <tr role="row">
                        <th>ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Companies</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($clients as $client)
                        @php /** @var App\Models\Client $client **/ @endphp
                        <tr>
                            <th>
                                {{ $client->id }}
                            </th>
                            <th>
                                <a href="{{ route('clients.edit', $client) }}">{{ $client->first_name }}</a>
                            </th>
                            <th>
                                {{ $client->last_name }}
                            </th>
                            <th>
                                {{ $client->companies->pluck('name')->implode(', ') }}
                            </th>
                            <th>
                                {{ $client->created_at }}
                            </th>
                            <th>
                                {{ $client->updated_at ?? '' }}
                            </th>
                            <th class="text-right">
                                <form
                                    onsubmit="return confirm('Delete category?');"
                                    action="{{ route('clients.destroy', $client) }}"
                                    method="post"
                                >
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </th>
                        </tr>
                    @empty
                        <tr>
                            <th colspan="3">No data</th>
                        </tr>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3" class="pagination"> {{ $clients->links() }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection